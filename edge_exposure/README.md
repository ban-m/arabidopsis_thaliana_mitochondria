# Edge exposure to collect small and enriched amplicons

## Synopsis
```
cargo run --release -- [sam file] > [result].tsv
```

## Output format
Tab delimitered text: [iteration number] [read ID]