## Read Coverage Assembly

- Extract read with specified threshold(from all vs all assembly)
- This is recommended because we can not and should not have an a pri ori knowledge about the sequence of mitochondria.

### Notes

As Arimura sensei suggested, the coverage of chroloplast is several times bigger than that of mitochondira. Nonetheless, we can combine "wet" approach and "dry" approach to correctly extract mitochondrial genome from semi-purified samples.


## Validity checking

