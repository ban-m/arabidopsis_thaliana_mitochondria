# Stiff structure from ava

Date: 2019/05/13

Author: Bansho Masutnai

Email: ban-m@g.ecc.u-tokyo.ac.jp

## TL;DR

The aim of this repository is to find repetitive strucrures from input (fasta/fastq) file with the aid of all-vs-all alingers.

The term "finding repeat structure" is vague, as it can be interpreted as 