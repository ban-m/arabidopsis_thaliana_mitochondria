# decompose MSA


## Synopsis

```bash
cargo run --release --bin generate_msa -- [length of read] [# of sequence] [variation rate] [mixing rate] [substituion rate] [answer output file] > [ASCII MSA]